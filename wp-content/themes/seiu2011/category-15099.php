<?php
/*
Archive Template: Slider for Category 15099 http://www.seiu1.org/category/stand-for-security/ */ 
?>
 
<?php get_header(); ?>

<div class="yui-gc">
	<div class="yui-u first maincol bfr bfr-home" role="main">
	
		<div class="bfr-b banner">
		
	<div id='tmpSlideshow'>
		
	
			<?php
			$my_query = new WP_Query('cat=15099&tag=section-banner1&posts_per_page=1');
			while ($my_query->have_posts()) : $my_query->the_post(); ?>
		<div id='tmpSlide-1' class='tmpSlide'>
			<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('slide'); ?></a>
		<div class='tmpSlideCopy'>
    	    <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
	        <?php the_excerpt();?> <a href="<?php the_permalink(); ?>" class="read-more">Continue&nbsp;reading&nbsp;<span class="meta-nav">&rarr;</span></a>
		</div><!-- END .tmpSlideCopy -->
			<?php endwhile; ?> 
	    </div><!-- END .tmpSlide -->
    


			<?php
			$my_query = new WP_Query('cat=15099&tag=section-banner2&posts_per_page=1');
			while ($my_query->have_posts()) : $my_query->the_post(); ?>    
    <div id='tmpSlide-2' class='tmpSlide'>
      <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('slide'); ?></a>  
      <div class='tmpSlideCopy'>
        <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
        <?php the_excerpt();?> <a href="<?php the_permalink(); ?>" class="read-more">Continue&nbsp;reading&nbsp;<span class="meta-nav">&rarr;</span></a>
      </div><!-- END .tmpSlideCopy -->
      <?php endwhile; ?>
    </div><!-- END .tmpSlide -->



			<?php
			$my_query = new WP_Query('cat=15099&tag=section-banner3&posts_per_page=1');
			while ($my_query->have_posts()) : $my_query->the_post(); ?>
    <div id='tmpSlide-3' class='tmpSlide'>
		     <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('slide'); ?></a>       
      <div class='tmpSlideCopy'>
        <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
        <?php the_excerpt();?> <a href="<?php the_permalink(); ?>" class="read-more">Continue&nbsp;reading&nbsp;<span class="meta-nav">&rarr;</span></a>
        
      </div><!-- END .tmpSlideCopy -->
       <?php endwhile; ?>
    	</div><!-- END .tmpSlide -->
    
    
    
   			<?php
			$my_query = new WP_Query('cat=15099&tag=section-banner4&posts_per_page=1');
			while ($my_query->have_posts()) : $my_query->the_post(); ?>	
        <div id='tmpSlide-4' class='tmpSlide'>
       <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('slide'); ?></a> 
      <div class='tmpSlideCopy'>
        <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
        <?php the_excerpt();?> <a href="<?php the_permalink(); ?>" class="read-more">Continue&nbsp;reading&nbsp;<span class="meta-nav">&rarr;</span></a>
      </div><!-- END .tmpSlideCopy -->
      <?php endwhile; ?>
    </div><!-- END .tmpSlide -->
    
    
    
    
   			 <?php
			$my_query = new WP_Query('cat=15099&tag=section-banner5&posts_per_page=1');
			while ($my_query->have_posts()) : $my_query->the_post(); ?>	
     <div id='tmpSlide-5' class='tmpSlide'>
       <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('slide'); ?></a> 
      <div class='tmpSlideCopy'>
        <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
        <?php the_excerpt();?> <a href="<?php the_permalink(); ?>" class="read-more">Continue&nbsp;reading&nbsp;<span class="meta-nav">&rarr;</span></a>
      </div><!-- END .tmpSlideCopy -->
      <?php endwhile; ?>
    </div><!-- END .tmpSlide -->    
    
    <div id='tmpSlideshowControls'>
      <div class='tmpSlideshowControl' id='tmpSlideshowControl-1'><span>1</span></div>
      <div class='tmpSlideshowControl' id='tmpSlideshowControl-2'><span>2</span></div>
      <div class='tmpSlideshowControl' id='tmpSlideshowControl-3'><span>3</span></div>
      <div class='tmpSlideshowControl' id='tmpSlideshowControl-4'><span>4</span></div>
      <div class='tmpSlideshowControl' id='tmpSlideshowControl-5'><span>5</span></div>
    </div><!-- END #tmpSlideshowControls -->
</div> <!-- END #tmpSlideshow -->
</div> <!-- END .bfr-b .banner -->
		
		
		
<div class="bfr-fr featured-recent yui-g">			
		
		
	<div class="bfr-left bfr-f featured yui-u first">
				<h4 class="bfr-head">Featured Post:</h4>
				<?php
					$my_query = new WP_Query('cat=15099&tag=section-featured&posts_per_page=1');
					while ($my_query->have_posts()) : $my_query->the_post(); 
				?>
				<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
				<div class="bfr-exc featured-exc">
					<?php the_excerpt();?> <a href="<?php the_permalink(); ?>" class="read-more">READ THE FULL POST <span class="meta-nav">&rarr;</span></a>
					
					<?php endwhile; ?>
					
					
					
					
				</div> <!-- END .bfr-exc .featured-exc -->
			</div> <!-- END .bfr-left .featured -->
			
			<div class="bfr-right bfr-r recent yui-u">
				<h4 class="bfr-head">Recent Items:</h4>
				<?php
					$my_query = new WP_Query('cat=15099&tag=section-recent&posts_per_page=5');
					while ($my_query->have_posts()) : $my_query->the_post();
				?>
				<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
				<?php endwhile;	?>
			</div> <!-- END .bfr-right .recent -->
		
		</div> <!-- END .bfr-fr .featured-recent .yui-g -->

		<div id="promo-container" class="yui-skin-sam promoset">
		<ol id="carousel">
			<?php $my_query = new WP_Query('cat=15099&tag=section-promo&posts_per_page=3');	while ($my_query->have_posts()) : $my_query->the_post(); ?>
			<li class="promo spacer">
				<div class="pr-img">
					<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail( 'promo' ); ?></a>
				</div>
				<div class="pr-text">
					<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
				</div>
			</li>
			<?php endwhile;	?>
		</ol> 
		</div> <!-- END #promo-container .yui-skin-sam .promoset -->
</div> <!-- END .yui-u .first .maincol .home -->

<?php get_sidebar(); ?>
</div> <!-- END .yui-gc -->
</div> <!-- END #bd -->
<?php get_footer(); ?>