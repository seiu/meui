<?php
/*
Archive Template: Press
*/ 
?>
<?php get_header(); ?>
<?php global $query_string; ?>

<div class="yui-gc">
	<div class="yui-u first maincol pr">
			
		<h1 class="page-title"><?php printf( __( '%s', 'twentyten' ), '<span>' . single_cat_title( '', false ) . '</span>' ); ?></h1>
		<?php $categorydesc = category_description(); if ( ! empty( $categorydesc ) ) echo apply_filters( 'archive_meta', '<div class="archive-meta">' . $categorydesc . '</div>' ); ?>


		<?php $my_query = new WP_Query($query_string . '&posts_per_page=1'); while ($my_query->have_posts()) : $my_query->the_post(); ?>

		<div class="pr-top">
			<div class="datebox">
			    <h2 class="press"><?php the_time('F'); ?> <?php the_time('j'); ?>, <?php the_time('Y'); ?><br /><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
            </div>
			<div class="pr-excerpt">
				
				<p class="press"><?php the_excerpt(); ?></p>
				<p align="right"><a class="morelink" href="<?php the_permalink(); ?>">Read more &raquo;</a></p>
			</div>
			<hr width="50%" align="center" />
			
		</div> <!-- END .pr-top -->
		<?php endwhile;	?>
		
		<div class="sec-pr">
			<?php $my_query = new WP_Query($query_string . '&posts_per_page=-1&offset=1'); while ($my_query->have_posts()) : $my_query->the_post();	?>	
			
			
			<div class="PRlistEntry" style="margin-bottom:10px;">
			
			
			
			<ul>
			<li><?php the_time('M d, Y'); ?> |</li>
			<li><div class="press-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div></li>
			</ul>
			</div>



			<?php endwhile;	?>	
		</div> <!-- END .sec-pr -->




	</div> <!-- END .yui-u .first .maincol-->
<?php get_sidebar(); ?>

</div> <!-- END .yui-gc -->
</div> <!-- END #bd -->
<?php get_footer(); ?>