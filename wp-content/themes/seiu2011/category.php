<?php
/**
 * The template used to display Category Archive pages
 *
 * @package WordPress
 * @subpackage Twenty Ten
 * @since 3.0.0
 */
?>

<?php get_header(); ?>

<div class="yui-gc">
	<div class="yui-u first maincol category">

				
		<h1 class="page-title"><?php printf( __( '%s', 'twentyten' ), '<span>' . single_cat_title( '', false ) . '</span>' ); ?></h1>
		<?php $categorydesc = category_description(); if ( ! empty( $categorydesc ) ) echo apply_filters( 'archive_meta', '<div class="archive-meta">' . $categorydesc . '</div>' ); ?>

		<?php
		/* Run the loop for the category page to output the posts.
		 * If you want to overload this in a child theme then include a file
		 * called loop-category.php and that will be used instead.
		 */
		get_template_part( 'loop', 'category' );
		?>






	</div> <!-- END .yui-u .first .maincol .category -->
<?php get_sidebar(); ?>
	
</div> <!-- END .yui-gc -->
</div> <!-- END #bd -->
<?php get_footer(); ?>