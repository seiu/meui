<?php
/*
Archive Template: Blog
*/ 
?>

<?php get_header(); ?>
<?php global $query_string; ?>
<div class="yui-gc">





	<div class="yui-u first maincol blog">
				
		<h1 class="page-title"><?php printf( __( '%s', 'twentyten' ), '<span>' . single_cat_title( '', false ) . '</span>' ); ?></h1>
		<?php $categorydesc = category_description(); if ( ! empty( $categorydesc ) ) echo apply_filters( 'archive_meta', '<div class="archive-meta">' . $categorydesc . '</div>' ); ?>
		<?php $my_query = new WP_Query($query_string . '&posts_per_page=20');
				while ($my_query->have_posts()) : $my_query->the_post();
				?>
		<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<h1 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Permalink to %s', 'twentyten' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark"><?php the_title(); ?></a></h1>
			<div class="entry-meta">
				<span class="meta-prep meta-prep-author"><?php _e( 'Posted by ', 'twentyten' ); ?></span>
				<span class="author vcard"><?php the_author(); ?> -
				
				<?php
				
				
      //$stored_listening_to will be the first value of the key 'listening_to'
      $stored_listening_to = get_post_meta( $post_id, 'myplugin_noncename', 'true' );

      if ( ! empty( $myplugin_noncename ) ) {
        $return .= '<strong>Other Author</strong>: ';
        $return .= $myplugin_noncename;
        }
				
				
				?>
				
				
				</span>
				
				
				
				<span class="meta-sep"><?php _e( ' on ', 'twentyten' ); ?> </span>
				<span class="entry-date"><?php echo get_the_date(); ?></span>
				<?php edit_post_link( __( 'Edit', 'twentyten' ), "<span class=\"meta-sep\">|</span>\n\t\t\t\t\t\t<span class=\"edit-link\">", "</span>\n\t\t\t\t\t" ); ?>
			</div> <!-- END .entry-meta -->

			<div class="entry-content">
				<?php the_content(); ?>
				<?php wp_link_pages( 'before=<div class="page-link">' . __( 'Pages:', 'twentyten' ) . '&after=</div>' ); ?>
			</div> <!-- END .entry-content -->

			<?php if ( get_the_author_meta( 'description' ) ) : // If a user has filled out their decscription show a bio on their entries  ?>
			
			<div id="entry-author-info">
				<div id="author-avatar">
					<?php echo get_avatar( get_the_author_meta( 'user_email' ), apply_filters( 'twentyten_author_bio_avatar_size', 60 ) ); ?>
				</div><!-- #author-avatar 	-->
				<div id="author-description">
					<h2><?php _e( 'About ', 'twentyten' ); ?><?php the_author(); ?></h2>
					<?php the_author_meta( 'description' ); ?>
					<div id="author-link">
						<?php the_author(); ?>
					</div><!-- #author-link	-->
				</div><!-- #author-description	-->
			</div><!-- .entry-author-info -->
			<?php endif; ?>

			<div class="entry-utility">
				<?php
				$tag_list = get_the_tag_list('', ', ');
				if ( '' != $tag_list ) {$utility_text = __( ' ', 'twentyten' );
				} else {$utility_text = __( '', 'twentyten' );
				}
				printf( $utility_text,
							get_the_category_list( ', ' ),
							$tag_list,
							get_permalink(),
							the_title_attribute( 'echo=0' ),
							get_post_comments_feed_link() 
				); 
				?>

				<?php edit_post_link( __( 'Edit', 'twentyten' ), "\n\t\t\t\t\t<span class=\"edit-link\">", "</span>" ); ?>
			</div><!-- END .entry-utility -->
		</div> <!-- END #post-<?php the_ID(); ?> .<?php post_class(); ?> -->
		<?php
			endwhile;
		?>
		
	<div class="navigation"><p><?php posts_nav_link(); ?></p></div>

	</div> <!-- END .yui-u .first .maincol .blog -->





	<?php get_sidebar(); ?>
</div> <!-- END .yui-gc -->

</div> <!-- END #bd -->
<?php get_footer(); ?>