<?php
/**
 * The template used to display Worksite Archive pages
 *
 * @package WordPress
 * @subpackage Twenty Eleven
 * @since 3.0.0
 */
?>

<?php get_header(); ?>

<div class="yui-gc">
	<div class="yui-u first maincol">

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

				<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<h1 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php the_time(); ?>" rel="bookmark"><?php the_title(); ?></a></h1>


					<div class="entry-content">
						<?php the_content(); ?>
						<?php wp_link_pages( 'before=<div class="page-link">' . __( 'Pages:', 'twentyten' ) . '&after=</div>' ); ?>
					</div><!-- .entry-content -->
					<div>
					
					<?php
						 echo get_the_term_list( $post->ID, 'steward', 'Stewards: ', ', ', '' );
					?>
					<br />
					Website: 
					<?php
						global $post;
						echo get_post_meta($post->ID, 'website', true);
					?>
					<br />Address: 
					<?php
						echo get_post_meta($post->ID, 'address', true);
					?>
					<br />City: 
					<?php
						echo get_post_meta($post->ID, 'city', true);
					?>
					<br />State: 
					<?php
						echo get_post_meta($post->ID, 'state', true);
					?>
					<br />Zip: 
					<?php
						echo get_post_meta($post->ID, 'zip', true);
					?>
					<br />Phone: 
					<?php
						echo get_post_meta($post->ID, 'phone', true);
					?>
					<br />Image: 
					<?php
						echo get_post_meta($post->ID, 'image', true);
					?><br />
					<br /><strong>Stewards:</strong><br /><br />
					<?php $terms = get_the_terms( $post->ID , 'steward' );
						if($terms) {
								foreach( $terms as $term ) {
									echo $term->name.'<br />';
									echo 'http://wordpress.localsonline.org/?steward=' . $term->slug.'<br />';
									echo $term->description.'<br /><br />';
								}
						}
					?>
					</div>


					<div class="entry-utility">
				<?php edit_post_link( __( 'Edit', 'twentyten' ), "\n\t\t\t\t\t<span class=\"edit-link\">", "</span>" ); ?>
					</div><!-- .entry-utility -->
					
				</div><!-- #post-<?php the_ID(); ?> -->
	
<?php
endwhile;
endif;
?>

	</div> <!-- END .yui-u .first .maincol-->
<?php get_sidebar(); ?>

</div> <!-- END .yui-gc -->
</div> <!-- END #bd -->
<?php get_footer(); ?>
