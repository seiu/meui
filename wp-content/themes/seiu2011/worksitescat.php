<?php
/*
Archive Template: Worksites Category Template
*/ 
?>
<?php get_header(); ?>
<?php global $query_string; ?>

<div class="yui-gc">
	<div class="yui-u first maincol">
				
<h1 class="page-title"><?php printf( __( '%s', 'twentyten' ), '<span>' . single_cat_title( '', false ) . '</span>' ); ?></h1>
<?php $categorydesc = category_description(); if ( ! empty( $categorydesc ) ) echo apply_filters( 'archive_meta', '<div class="archive-meta">' . $categorydesc . '</div>' ); ?>

		<?php
				$my_query = new WP_Query($query_string . '&orderby=title&order=asc&posts_per_page=-1');
				while ($my_query->have_posts()) : $my_query->the_post();
				?>
									
              <a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Permalink to %s', 'twentyten' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark"><?php the_title(); ?></a><br />



			

<?php if ( get_the_author_meta( 'description' ) ) : // If a user has filled out their decscription show a bio on their entries  ?>
				
<?php endif; ?>

			
			<?php
				endwhile;
			?>	


	</div> <!-- END .yui-u .first .maincol-->
<?php get_sidebar(); ?>

</div> <!-- END .yui-gc -->
</div> <!-- END #bd -->
<?php get_footer(); ?>