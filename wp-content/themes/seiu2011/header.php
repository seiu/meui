<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=7" />
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <link rel="shortcut icon" href="http://img.seiu.org/i/favicon.ico" >
    
	<title><?php
        if ( is_single() ) {
			single_post_title();
		} elseif ( is_home() || is_front_page() ) {
			bloginfo( 'name' ); echo ' | '; bloginfo( 'description' ); twentyten_the_page_number();
		} elseif ( is_page() ) {
			single_post_title( '' );
		} elseif ( is_search() ) {
			printf( __( 'Search results for "%s"', 'twentyten' ), esc_html( $s ) ); twentyten_the_page_number(); echo ' | '; bloginfo( 'name' ); 
		} elseif ( is_404() ) {
			_e( 'Not Found', 'twentyten' ); echo ' | '; bloginfo( 'name' );
		} else {
			wp_title( '' ); echo ' | '; bloginfo( 'name' ); twentyten_the_page_number();
		}
    ?></title>

	<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" >
	<link rel="stylesheet" type="text/css" media="print" href="<?php bloginfo( 'stylesheet_directory' ); ?>/print.css" >
	<script type="text/javascript" src="http://img.seiu.org/css/wp/js/livevalidation.js"></script>


	<?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>

	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" >

	<?php wp_head(); ?>

		<?php
// only load scripts into the templates that need them
if (is_home()) {
        // load the yui-carousel script on the home page
        echo "<script type='text/javascript' src='http://yui.yahooapis.com/combo?2.8.0r4/build/yuiloader-dom-event/yuiloader-dom-event.js&2.8.0r4/build/animation/animation-min.js&2.8.0r4/build/event-mouseenter/event-mouseenter-min.js&2.8.0r4/build/selector/selector-min.js&2.8.0r4/build/event-delegate/event-delegate-min.js&2.8.0r4/build/element/element-min.js&2.8.0r4/build/carousel/carousel-min.js'></script>";
} else {; 
// space for other scripts, other pages 

// only load scripts into the templates that need them
if (is_page(1124)) {
        // load the yui-carousel script on the home page
        echo "<script type='text/javascript' src='http://img.seiu.org/css/wp/js/www.cirseiu.org/dropdown.js'></script>";
} ; 
// only load scripts into the templates that need them
if (is_page(14872)) {
        // load the yui-carousel script on the home page
        echo "<script type='text/javascript' src='http://img.seiu.org/css/wp/js/www.cirseiu.org/dropdown.js'></script>";
} ; 
// only load scripts into the templates that need them
if (is_page(2408)) {
        // load the yui-carousel script on the home page
        echo "<script type='text/javascript' src='http://img.seiu.org/css/wp/js/www.cirseiu.org/dropdown.js'></script>";
} ; 


// only load scripts into the templates that need them
if (is_category(115938)) {
        // load the yui-carousel script on the home page
        echo "<script type='text/javascript' src='http://img.web-assets.co/css/wp/js/jquery/1.6.2/jquery.min.js'></script><script type='text/javascript' src='http://img.web-assets.co/css/wp/js/slider.js'></script>";
} ; 


// space for other scripts, other pages 

}
?>
<?php 
   if ( is_front_page() ){
?>
<script type="text/javascript" src="http://img.seiu.org/css/wp/js/jquery/1.6.2/jquery.min.js"></script>
<?php 
   }
   else {
?> 
   <!-- test -->
<?php  
   }
?>

<!-- call for slider -->
<script type="text/javascript" src="http://img.seiu.org/css/wp/js/jquery/1.6.2/jquery.min.js"></script>
<script type="text/javascript" src="http://img.seiu.org/css/wp/js/slider.js"></script>
<!-- end call for slider -->

<!-- Show Hide Accordion -->
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
<script>
$(function() {
$( "#accordion" ).accordion();
});
</script>
<!-- end Show Hide Accordion -->


</head>

<body <?php body_class(); ?>>
<div id="doc" class="hfeed seiu1020">
	<div id="hd">
		<div id="masthead">
			<div id="branding">
				<div id="site-title">
					<a href="<?php echo home_url( '/' ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
						<img src="<?php header_image(); ?>" width="<?php echo HEADER_IMAGE_WIDTH; ?>" height="<?php echo HEADER_IMAGE_HEIGHT; ?>" alt="<?php bloginfo( 'name' ); ?> - <?php bloginfo( 'description' ); ?>" >
					</a>
				</div> <!-- END #site-title -->
				<div id="header-widget">
					<?php dynamic_sidebar('topright-widget-area'); ?>
				</div> <!-- END #header-widget-area -->
			</div><!-- END #branding -->

			<div id="navbar">
				<div class="skip-link screen-reader-text">
					<a href="#content" title="<?php esc_attr_e( 'Skip to content', 'twentyten' ); ?>"><?php _e( 'Skip to content', 'twentyten' ); ?></a>
				</div> <!-- END .skip-link .screen-reader-text -->
					<?php wp_nav_menu( 'sort_column=menu_order&container_class=menu-header' ); ?>
					<?php // wp_nav_menu(array('theme_location' => 'top_nav', 'container_class' => 'menu-header')); ?>
			</div><!-- #navbar -->

		 	<div id="navbar2">
			 	<div class="skip-link screen-reader-text">
			 		<a href="#content" title="<?php esc_attr_e( 'Skip to content', 'twentyten' ); ?>"><?php _e( 'Skip to content', 'twentyten' ); ?></a>
				</div> <!-- END .skip-link .screen-reader-text -->
			 		<?php dynamic_sidebar('subnav-widget-area'); ?>
			 		<?php //  wp_nav_menu( array( 'menu' => 'three', 'container_class' => 'menu-header') ); ?>
			</div> <!-- #navbar2 -->
		</div> <!-- #masthead -->
	</div> <!-- #hd -->
	<div id="bd">
