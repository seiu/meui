<?php
/**
 * The template used to display Tag Archive pages
 *
 * @package WordPress
 * @subpackage Twenty Ten
 * @since 3.0.0
 */
?>

<?php get_header(); ?>

<div class="yui-gc">
	<div class="yui-u first maincol">

<?php the_post(); ?>

<h1 class="page-title"><?php printf( __( 'Tag Archives: %s', 'twentyten' ), '<span>' . single_tag_title( '', false ) . '</span>' ); ?></h1>

<?php rewind_posts(); ?>

<?php
/* Run the loop for the tag archive to output the posts
 * If you want to overload this in a child theme then include a file
 * called loop-tag.php and that will be used instead.
 */
 get_template_part( 'loop', 'tag' );
?>



	</div> <!-- END .yui-u .first .maincol-->
<?php get_sidebar(); ?>

</div> <!-- END .yui-gc -->
</div> <!-- END #bd -->
<?php get_footer(); ?>