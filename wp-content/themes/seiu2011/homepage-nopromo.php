<?php
/*
Template Name: Home - No Promo
Description: Home - No Promo at all
*/
?>
 
<?php get_header(); ?>

<div class="yui-gc">
	<div class="yui-u first maincol bfr bfr-home" role="main">
	
		<div class="bfr-b banner">
			<?php
				$my_query = new WP_Query('tag=front-banner&posts_per_page=1');
				while ($my_query->have_posts()) : $my_query->the_post(); ?>
			<h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
			<div class="banner-img">
				<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('large'); ?></a>
			</div>
			<div class="bfr-exc banner-exc">
				<?php the_excerpt();?> <a href="<?php the_permalink(); ?>" class="read-more">Continue&nbsp;reading&nbsp;<span class="meta-nav">&rarr;</span></a>	
				<?php endwhile; ?>
			</div> <!-- END .bfr-exc .banner-exc -->
		</div> <!-- END .bfr-b .banner -->
		
		<div class="bfr-fr featured-recent yui-g">	
		
			<div class="bfr-left bfr-f featured yui-u first">
				<h4 class="bfr-head">Featured Post:</h4>
				<?php
					$my_query = new WP_Query('tag=front-featured&posts_per_page=1');
					while ($my_query->have_posts()) : $my_query->the_post(); 
				?>
				<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
				<div class="bfr-exc featured-exc">
					<?php the_excerpt();?> <a href="<?php the_permalink(); ?>" class="read-more">READ THE FULL POST <span class="meta-nav">&rarr;</span></a>
					
					<?php endwhile; ?>
					
					
					
					
				</div> <!-- END .bfr-exc .featured-exc -->
			</div> <!-- END .bfr-left .featured -->
			
			<div class="bfr-right bfr-r recent yui-u">
				<h4 class="bfr-head">Recent Items:</h4>
				<?php
					$my_query = new WP_Query('tag=front-recent&posts_per_page=5');
					while ($my_query->have_posts()) : $my_query->the_post();
				?>
				<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
				<?php endwhile;	?>
			</div> <!-- END .bfr-right .recent -->
		
		</div> <!-- END .bfr-fr .featured-recent .yui-g -->

	
	

</div> <!-- END .yui-u .first .maincol .home -->

<?php get_sidebar(); ?>
</div> <!-- END .yui-gc -->
</div> <!-- END #bd -->
<?php get_footer(); ?>