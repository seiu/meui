<?php get_header(); ?>

<div class="yui-gc">
	<div class="yui-u first maincol">

		<?php the_post(); ?>

		<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<h1 class="entry-title"><?php the_title(); ?></h1>
			<div class="entry-meta">
				<span class="meta-prep meta-prep-author">
					<?php _e( 'Posted by ', 'twentyten' ); ?>
				</span>
				<span class="author vcard"><?php the_author(); ?></span>
				<span class="meta-sep">
					<?php _e( ' on ', 'twentyten' ); ?>
				</span>
				<a href="<?php the_permalink(); ?>" title="<?php the_time(); ?>" rel="bookmark">
					<span class="entry-date"><?php echo get_the_date(); ?></span>
				</a>
				<?php edit_post_link( __( 'Edit', 'twentyten' ), "<span class=\"meta-sep\">|</span>\n\t\t\t\t\t\t<span class=\"edit-link\">", "</span>\n\t\t\t\t\t" ); ?>
			</div> <!-- END .entry-meta -->

			<div class="entry-content">
				<?php the_content(); ?>
				<?php wp_link_pages( 'before=<div class="page-link">' . __( 'Pages:', 'twentyten' ) . '&after=</div>' ); ?>
			</div><!-- END .entry-content -->
				
			<?php if ( get_the_author_meta( 'description' ) ) : // If a user has filled out their decscription show a bio on their entries  ?>
			
			<div id="entry-author-info">
				<div id="author-avatar">
					<?php echo get_avatar( get_the_author_meta( 'user_email' ), apply_filters( 'twentyten_author_bio_avatar_size', 60 ) ); ?>
				</div><!-- END #author-avatar 	-->
				<div id="author-description">
					<h2><?php _e( 'About ', 'twentyten' ); ?><?php the_author(); ?></h2>
					
					<?php the_author_meta( 'description' ); ?>

					<div id="author-link">
						<?php the_author(); ?>
					</div> <!-- END #author-link -->
				</div><!-- END #author-description -->
			</div><!-- END .entry-author-info -->
				
			<?php endif; ?>

			<div class="entry-utility">

				<?php
					$tag_list = get_the_tag_list('', ', ');
					if ( '' != $tag_list ) {
						$utility_text = __( '', 'twentyten' );
					} else {
						$utility_text = __( '', 'twentyten' );
					}
					printf( 
						$utility_text,
						get_the_category_list( ', ' ),
						$tag_list,
						get_permalink(),
						the_title_attribute( 'echo=0' ),
						get_post_comments_feed_link() 
					); 
				?>

				<?php edit_post_link( __( 'Edit', 'twentyten' ), "\n\t\t\t\t\t<span class=\"edit-link\">", "</span>" ); ?>

			</div><!-- END .entry-utility -->

		</div> <!-- END #post-<?php the_ID(); ?> -->

		<?php comments_template( '', true ); ?>

	</div> <!-- END .yui-u .first .maincol-->
<?php get_sidebar(); ?>

</div> <!-- END .yui-gc -->
</div> <!-- END #bd -->


<?php get_footer(); ?>