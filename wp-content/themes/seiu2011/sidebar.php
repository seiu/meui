<div class="yui-u sidebar">

	<div id="take-action" class="widget-area">
		
		<?php

		if ( ! dynamic_sidebar( 'take-action-widget-area' ) ) :  ?>

						
		<?php endif; // end take-action widget area ?>
	
	</div> <!-- END #take-action .widget-area -->

	<div id="primary" class="widget-area">
		<?php
	
			if ( ! dynamic_sidebar( 'primary-widget-area' ) ) : ?>
		
			<?php endif; // end primary widget area ?>

	</div> <!-- END #primary .widget-area -->
		
	<?php if ( is_active_sidebar( 'secondary-widget-area' ) ) : ?>

	<div id="secondary" class="widget-area">
			<?php dynamic_sidebar( 'secondary-widget-area' ); ?>
	</div> <!-- END #secondary .widget-area -->

	<?php endif; ?>

</div> <!-- END .yui-u .sidebar -->