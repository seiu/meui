<?php
// remove_filter( 'term_description', 'wp_kses_data' );
// remove_filter( 'pre_term_description', 'wp_filter_kses' ); /*added to allow tag images and html*/

// add_action ( 'edit_tag_form_fields', 'your_input_box');
// add_action ( 'add_tag_form_fields', 'your_input_box');

// add_action('created_' . $tax->name, 'your_input_box', 1, 2);
// add_action('edited_' . $tax->name, 'your_input_box', 1, 2);






// begin










// register top nav space for custom menu
function seiu2011_register_menus()
{

	register_nav_menu('top_nav', 'Top Navigation Menu');

}

add_action('init', 'seiu2011_register_menus');


/* Use the admin_menu action to define the custom boxes */
add_action('admin_menu', 'myplugin_add_custom_box');

/* Use the save_post action to do something with the data entered */
add_action('save_post', 'myplugin_save_postdata');

/* Adds a custom section to the "advanced" Post and Page edit screens */
function myplugin_add_custom_box() {

  if( function_exists( 'add_meta_box' )) {
    add_meta_box( 'myplugin_sectionid', __( 'Other Author', 'myplugin_textdomain' ), 
                'myplugin_inner_custom_box', 'post', 'side' );
   } else {
    add_action('dbx_post_advanced', 'myplugin_old_custom_box' );
  }
}
   
/* Prints the inner fields for the custom post/page section */
function myplugin_inner_custom_box() {

  // Use nonce for verification

  echo '<input type="hidden" name="myplugin_noncename" id="myplugin_noncename" value="' . 
    wp_create_nonce( plugin_basename(__FILE__) ) . '" />';

  // The actual fields for data entry

  echo '<label for="myplugin_new_field">' . __("whatever", 'myplugin_textdomain' ) . '</label> ';
  echo '<input type="text" name="myplugin_new_field" value="whatever" size="25" />';
}

/* Prints the edit form for pre-WordPress 2.5 post/page */
function myplugin_old_custom_box() {

  echo '<div class="dbx-b-ox-wrapper">' . "\n";
  echo '<fieldset id="myplugin_fieldsetid" class="dbx-box">' . "\n";
  echo '<div class="dbx-h-andle-wrapper"><h3 class="dbx-handle">' . 
        __( 'My Post Section Title', 'myplugin_textdomain' ) . "</h3></div>";   
   
  echo '<div class="dbx-c-ontent-wrapper"><div class="dbx-content">';

  // output editing form

  myplugin_inner_custom_box();

  // end wrapper

  echo "</div></div></fieldset></div>\n";
}

/* When the post is saved, saves our custom data */
function myplugin_save_postdata( $post_id ) {

  // verify this came from the our screen and with proper authorization,
  // because save_post can be triggered at other times

  if ( !wp_verify_nonce( $_POST['myplugin_noncename'], plugin_basename(__FILE__) )) {
    return $post_id;
  }

  // verify if this is an auto save routine. If it is our form has not been submitted, so we dont want
  // to do anything
  if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) 
    return $post_id;

  
  // Check permissions
  if ( 'post' == $_POST['post_type'] ) {
    if ( !current_user_can( 'edit_post', $post_id ) )
      return $post_id;
  }

  // OK, we're authenticated: we need to find and save the data

  $mydata = $_POST['myplugin_new_field'];

  // Do something with $mydata 
  // probably using add_post_meta(), update_post_meta(), or 
  // a custom table (see Further Reading section below)

   return $mydata;
}



// end



// continue reading code after the excerpt (type what you want it to print in the quotes after 'return'
function new_excerpt_more($more) {
	return ' ...';
}
add_filter('excerpt_more', 'new_excerpt_more');
// continue reading code after the excerpt END



function remove_dashboard_widgets() {
	// Globalize the metaboxes array, this holds all the widgets for wp-admin

	global $wp_meta_boxes;

	// Remove the incomming links widget
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']);
	
	// Remove the plugins links widget
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);
		
	// Remove the plugins links widget
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
		
	// Remove the plugins links widget
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);	
	
} 

// Hoook into the 'wp_dashboard_setup' action to register our function

add_action('wp_dashboard_setup', 'remove_dashboard_widgets' );

// Set the content width based on the Theme CSS
if ( ! isset( $content_width ) )
	$content_width = 640;

if ( ! function_exists( 'twentyten_init' ) ) :
function twentyten_init() {
	// Your Changeable header business starts here
	// No CSS, just IMG call
	define( 'HEADER_TEXTCOLOR', '' );
	define( 'HEADER_IMAGE', '%s/images/headers/locals2011-bg_010.jpg' ); // %s is theme dir uri
	define( 'HEADER_IMAGE_WIDTH', apply_filters( 'twentyten_header_image_width',  564 ) );
	define( 'HEADER_IMAGE_HEIGHT', apply_filters( 'twentyten_header_image_height',	106 ) );
	define( 'NO_HEADER_TEXT', true );

	add_custom_image_header( '', 'twentyten_admin_header_style' );
	// and thus ends the changeable header business

	register_default_headers( array (
		'temp' => array (
			'url' => '%s/images/headers/templogo.gif',
			'thumbnail_url' => '%s/images/headers/templogo-thumbnail.gif',
			'description' => __( 'Temp', 'twentyten' )
		),
		'seiu' => array (
			'url' => '%s/images/headers/seiu-org-logo.gif',
			'thumbnail_url' => '%s/images/headers/seiu-org-logo-thumbnail.gif',
			'description' => __( 'SEIU', 'twentyten' )
		)
	) );

	add_custom_background();

	// This theme styles the visual editor with editor-style.css to match the theme style.
	add_editor_style();

	// This theme needs post thumbnails
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 50, 50, true ); // Normal post thumbnails
	add_image_size( 'promo', 186, 106, true ); // Promo thumbnail size
	add_image_size( 'region', 275, 140, true ); // image size for region category template
	add_image_size( 'slide', 340, 250, true ); // thumbnail size for slider
	add_image_size( 'feature', 180, 100, true ); // featured image for 925
	add_image_size( 'slide925', 580, 250, true ); // thumbnail size for slider925
	add_image_size( 'cronycards', 186, 300, true ); // thumbnail size promo crony card slider Wisconsin Jobs Now
	add_image_size( 'halfwidth', 290, 300, true ); // thumbnail size for NHPrisonWatch 


	// This theme uses wp_nav_menu()
	add_theme_support( 'nav-menus' );

	// We'll be using them for custom header images on posts and pages
	// so we want them to be 940 pixels wide by 198 pixels tall (larger images will be auto-cropped to fit)
	set_post_thumbnail_size( HEADER_IMAGE_WIDTH, HEADER_IMAGE_HEIGHT, true );

	// Add default posts and comments RSS feed links to head
	add_theme_support( 'automatic-feed-links' );

	// Make theme available for translation
	// Translations can be filed in the /languages/ directory
	load_theme_textdomain( 'twentyten', TEMPLATEPATH . '/languages' );

	$locale = get_locale();
	$locale_file = TEMPLATEPATH . "/languages/$locale.php";
	if ( is_readable( $locale_file ) )
		require_once( $locale_file );
}
endif;
add_action( 'after_setup_theme', 'twentyten_init' );

if ( ! function_exists( 'twentyten_admin_header_style' ) ) :
function twentyten_admin_header_style() {
?>
<style type="text/css">
#headimg {
	height: <?php echo HEADER_IMAGE_HEIGHT; ?>px;
	width: <?php echo HEADER_IMAGE_WIDTH; ?>px;
}
#headimg h1, #headimg #desc {
	display: none;
}
</style>
<?php
}
endif;

// Get the page number
if ( ! function_exists( 'twentyten_get_page_number' ) ) :
function twentyten_get_page_number() {
	if ( get_query_var( 'paged' ) )
		return ' | ' . __( 'Page ' , 'twentyten' ) . get_query_var( 'paged' );
}
endif;

// Echo the page number
if ( ! function_exists( 'twentyten_the_page_number' ) ) :
function twentyten_the_page_number() {
	echo twentyten_get_page_number();
}
endif;

// Control excerpt length
if ( ! function_exists( 'twentyten_excerpt_length' ) ) :
function twentyten_excerpt_length( $length ) {
	return 40;
}
endif;
add_filter( 'excerpt_length', 'twentyten_excerpt_length' );


// Template for comments and pingbacks
if ( ! function_exists( 'twentyten_comment' ) ) :
function twentyten_comment( $comment, $args, $depth ) {
	$GLOBALS ['comment'] = $comment; ?>
	<?php if ( '' == $comment->comment_type ) : ?>
	<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
		<div id="comment-<?php comment_ID(); ?>">
		<div class="comment-author vcard">
			<?php echo get_avatar( $comment, 40 ); ?>
			<?php printf( __( '<cite class="fn">%s</cite> <span class="says">says:</span>', 'twentyten' ), get_comment_author_link() ); ?>
		</div>
		<?php if ( $comment->comment_approved == '0' ) : ?>
			<em><?php _e( 'Your comment is awaiting moderation.', 'twentyten' ); ?></em>
			<br>
		<?php endif; ?>

		<div class="comment-meta commentmetadata"><a href="<?php echo esc_url( get_comment_link( $comment->comment_ID ) ); ?>"><?php printf( __( '%1$s at %2$s', 'twentyten' ), get_comment_date(),  get_comment_time() ); ?></a><?php edit_comment_link( __( '(Edit)', 'twentyten' ),'  ','' ); ?></div>

		<div class="comment-body"><?php comment_text(); ?></div>

		<div class="reply">
			<?php comment_reply_link( array_merge( $args, array( 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
		</div>
	</div>

	<?php else : ?>
	<li class="post pingback">
		<p><?php _e( 'Pingback: ', 'twentyten' ); ?><?php comment_author_link(); ?><?php edit_comment_link ( __('edit', 'twentyten'), '&nbsp;&nbsp;', '' ); ?></p>
	<?php endif;
}
endif;

// unregister some default WP Widgets
function unregister_default_wp_widgets() {

	unregister_widget('WP_Widget_Meta');

	unregister_widget('WP_Widget_Recent_Comments');

}
 
add_action('widgets_init', 'unregister_default_wp_widgets', 1);

// Remove inline styles on gallery shortcode
if ( ! function_exists( 'twentyten_remove_gallery_css' ) ) :
function twentyten_remove_gallery_css( $css ) {
	return preg_replace( "#<style type='text/css'>(.*?)</style>#s", '', $css );
}
endif;
add_filter( 'gallery_style', 'twentyten_remove_gallery_css' );

if ( ! function_exists( 'twentyten_cat_list' ) ) :
function twentyten_cat_list() {
	return twentyten_term_list( 'category', ', ', __( '', 'twentyten' ), __( '', 'twentyten' ) );
}
endif;

if ( ! function_exists( 'twentyten_tag_list' ) ) :
function twentyten_tag_list() {
	return twentyten_term_list( 'post_tag', ', ', __( '', 'twentyten' ), __( '', 'twentyten' ) );
}
endif;

if ( ! function_exists( 'twentyten_term_list' ) ) :
function twentyten_term_list( $taxonomy, $glue = ', ', $text = '', $also_text = '' ) {
	global $wp_query, $post;
	$current_term = $wp_query->get_queried_object();
	$terms = wp_get_object_terms( $post->ID, $taxonomy );
	// If we're viewing a Taxonomy page.. 
	if ( isset( $current_term->taxonomy ) && $taxonomy == $current_term->taxonomy ) {
		// Remove the term from display.
		foreach ( (array) $terms as $key => $term ) {
			if ( $term->term_id == $current_term->term_id ) {
				unset( $terms[$key] );
				break;
			}
		}
		// Change to Also text as we've now removed something from the terms list.
		$text = $also_text;
	}
	$tlist = array();
	$rel = 'category' == $taxonomy ? 'rel="category"' : 'rel="tag"';
	foreach ( (array) $terms as $term ) {
		$tlist[] = '<a href="' . get_term_link( $term, $taxonomy ) . '" title="' . esc_attr( sprintf( __( 'View all posts in %s', 'twentyten' ), $term->name ) ) . '" ' . $rel . '>' . $term->name . '</a>';
	}
	if ( ! empty( $tlist ) )
		return sprintf( $text, join( $glue, $tlist ) );
	return '';
}
endif;





// Register widgetized areas
if ( ! function_exists( 'twentyten_widgets_init' ) ) :
function twentyten_widgets_init() {
	// Area 1 -------------------------- TOP RIGHT OF HEADER
	register_sidebar( array (
		'name' => 'Header',
		'id' => 'topright-widget-area',
		'description' => __( 'The right side of the site header (signup form and search recommended) Please do not include "Title:" for SEARCH widget in this slot' , 'twentyten' ),
		'before_widget' => '<div class="widget widget-topright">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	// Area 2 -------------------------- SUBNAVIGATION
	register_sidebar( array (
		'name' => 'Navigation 2',
		'id' => 'subnav-widget-area',
		'description' => __( 'The secondary navigation bar. To edit primary navigation bars, visit Appearance > Menus at far left' , 'twentyten' ),
		'before_widget' => '<div class="menu-header widget-subnav">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	// Area 3.1 -------------------------- SIDEBAR: TAKE ACTION
	register_sidebar( array (
		'name' => 'Sidebar 1 - Take Action',
		'id' => 'take-action-widget-area',
		'description' => __( 'ONLY the Take Action item widget works here' , 'twentyten' ),
		'before_widget' => '',
		'after_widget' => '',
		'before_title' => '',
		'after_title' => '',
	) );
	// Area 3.2 -------------------------- SIDEBAR: PRIMARY
	register_sidebar( array (
		'name' => 'Sidebar 2 - Everything Else',
		'id' => 'primary-widget-area',
		'description' => __( 'The rest of the sidebar (everything except the Take Action widget)' , 'twentyten' ),
		'before_widget' => '<div id="%1$s" class="widget widget-primary %2$s">',
		'after_widget' => "</div>",
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	
	
	
	// Area 4 -------------------------- FOOTER - 1
	register_sidebar( array (
		'name' => 'Footer 1',
		'id' => 'first-footer-widget-area',
		'description' => __( 'Column 1 of 4 possible footer areas' , 'twentyten' ),
		'before_widget' => '<div id="%1$s" class="widget widget-footer1 %2$s">',
		'after_widget' => "</div>",
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

	// Area 5
	register_sidebar( array (
		'name' => 'Footer 2',
		'id' => 'second-footer-widget-area',
		'description' => __( 'Column 2 of 4 possible footer areas' , 'twentyten' ),
		'before_widget' => '<div id="%1$s" class="widget widget-footer2 %2$s">',
		'after_widget' => "</div>",
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

	// Area 6
	register_sidebar( array (
		'name' => 'Footer 3',
		'id' => 'third-footer-widget-area',
		'description' => __( 'Column 3 of 4 possible footer areas' , 'twentyten' ),
		'before_widget' => '<div id="%1$s" class="widget widget-footer3 %2$s">',
		'after_widget' => "</div>",
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

	// Area 7
	register_sidebar( array (
		'name' => 'Footer 4',
		'id' => 'fourth-footer-widget-area',
		'description' => __( 'Column 4 of 4 possible footer areas' , 'twentyten' ),
		'before_widget' => '<div id="%1$s" class="widget widget-footer4 %2$s">',
		'after_widget' => "</div>",
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

	// Area 8
	register_sidebar( array (
		'name' => 'Splash Pop-Up',
		'id' => 'splash-widget-area',
		'description' => __( 'For placing widgets into the splash pop-up' , 'twentyten' ),
		'before_widget' => '',
		'after_widget' => '',
		'before_title' => '<div id="splashTitle">',
		'after_title' => '</div>',
	) );
	// Area 3.3 -------------------------- Floater Widget Area
	register_sidebar( array (
		'name' => 'Requested Widget Positioning',
		'id' => 'float-widget-area',
		'description' => __( 'Please ask New Media for special positioning request to use this widget space' , 'twentyten' ),
		'before_widget' => '<div id="%1$s" class="widget widget-primary %2$s">',
		'after_widget' => "</div>",
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	// Area 3.4 -------------------------- Floater Widget Area
	register_sidebar( array (
		'name' => 'Requested Widget Positioning 2',
		'id' => 'float-widget-area2',
		'description' => __( 'Please ask New Media for special positioning request to use this widget space' , 'twentyten' ),
		'before_widget' => '<div id="%1$s" class="widget widget-primary %2$s">',
		'after_widget' => "</div>",
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );


}
endif;
add_action( 'init', 'twentyten_widgets_init' );
