	<div id="ft">
		<div id="colophon">
			<?php get_sidebar( 'footer' ); ?>
			<div id="site-info">
				<a href="<?php echo home_url( '/' ) ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a>
			</div>
		</div><!-- END #colophon -->
	</div><!-- #ft -->
</div><!-- END #doc .hfeed -->

<?php wp_footer(); ?>

</body>
</html>