<?php
/**
 * Generic template for Archive pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty Ten
 * @since 3.0.0
 */
?>

<?php get_header(); ?>

<div class="yui-gc">
	<div class="yui-u first maincol">

<?php the_post(); ?>

<?php
	if (is_taxonomy('steward') == true) {
		echo term_description( '', get_query_var( 'taxonomy' ) );
	}
?>

<?php if ( is_day() ) : ?>
				<h1 class="page-title"><?php printf( __( '<span>%s</span>', 'twentyten' ), get_the_date() ); ?></h1>
<?php elseif ( is_month() ) : ?>
				<h1 class="page-title"><?php printf( __( '<span>%s</span>', 'twentyten' ), get_the_date('F Y') ); ?></h1>
<?php elseif ( is_year() ) : ?>
				<h1 class="page-title"><?php printf( __( '<span>%s</span>', 'twentyten' ), get_the_date('Y') ); ?></h1>
<?php else : ?>
				<h1 class="page-title"><?php _e( 'Blog Archives', 'twentyten' ); ?></h1>
<?php endif; ?>

<?php rewind_posts(); ?>
<?php
	/* Run the loop for the archives page to output the posts.
	 * If you want to overload this in a child theme then include a file
	 * called loop-archives.php and that will be used instead.
	 */
	 get_template_part( 'loop', 'archive' );
?>

	</div> <!-- END .yui-u .first .maincol-->
<?php get_sidebar(); ?>

</div> <!-- END .yui-gc -->
</div> <!-- END #bd -->
<?php get_footer(); ?>