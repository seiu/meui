<?php
/*
Archive Template: Resources
*/ 
?>

<?php get_header(); ?>
<?php global $query_string; ?>
<div class="yui-gc">





	<div class="yui-u first maincol blog">
				
		<h1 class="page-title"><?php printf( __( '%s', 'twentyten' ), '<span>' . single_cat_title( '', false ) . '</span>' ); ?></h1>
		<?php $categorydesc = category_description(); if ( ! empty( $categorydesc ) ) echo apply_filters( 'archive_meta', '<div class="archive-meta">' . $categorydesc . '</div>' ); ?>
		<?php $my_query = new WP_Query($query_string . '&posts_per_page=20');
				while ($my_query->have_posts()) : $my_query->the_post();
				?>
		<div id="post-<?php the_ID(); ?>" <?php post_class(); ?> style="margin: 0;">
		
		
		<h2 class="resource-title"><?php the_title(); ?></h2>
			<div class="resource">
				<?php the_content(); ?>
				<?php wp_link_pages( 'before=<div class="page-link">' . __( 'Pages:', 'twentyten' ) . '&after=</div>' ); ?>
			</div> <!-- END .resource -->



			<?php if ( get_the_author_meta( 'description' ) ) : // If a user has filled out their decscription show a bio on their entries  ?>
			
		
			<?php endif; ?>
			<div class="entry-utility">
				<?php
				$tag_list = get_the_tag_list('', ', ');
				if ( '' != $tag_list ) {$utility_text = __( ' ', 'twentyten' );
				} else {$utility_text = __( '', 'twentyten' );
				}
				printf( $utility_text,
							get_the_category_list( ', ' ),
							$tag_list,
							get_permalink(),
							the_title_attribute( 'echo=0' ),
							get_post_comments_feed_link() 
				); 
				?>

				<?php edit_post_link( __( 'Edit', 'twentyten' ), "\n\t\t\t\t\t<span class=\"edit-link\">", "</span>" ); ?>
			</div><!-- END .entry-utility -->
		
		</div> <!-- END #post-<?php the_ID(); ?> .<?php post_class(); ?> -->
		<?php
			endwhile;
		?>




		
	<div class="navigation"><p><?php posts_nav_link(); ?></p></div>


	</div> <!-- END .yui-u .first .maincol .blog -->





	<?php get_sidebar(); ?>
</div> <!-- END .yui-gc -->

</div> <!-- END #bd -->
<?php get_footer(); ?>