<?php
/*
Archive Template: Titles
*/ 
?>
<?php get_header(); ?>
<?php global $query_string; ?>

<div class="yui-gc">
	<div class="yui-u first maincol">
				
				<h1 class="page-title"><?php printf( __( '%s', 'twentyten' ), '<span>' . single_cat_title( '', false ) . '</span>' ); ?></h1>
				<?php $categorydesc = category_description(); if ( ! empty( $categorydesc ) ) echo apply_filters( 'archive_meta', '<div class="archive-meta">' . $categorydesc . '</div>' ); ?>
				<?php
				$my_query = new WP_Query($query_string . '&posts_per_page=20');
				while ($my_query->have_posts()) : $my_query->the_post();
				?>
				
				<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<h2 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Permalink to %s', 'twentyten' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark"><?php the_title(); ?></a></h2>

					<div class="entry-meta">
						<span class="meta-prep meta-prep-author"><?php _e( 'Posted by ', 'twentyten' ); ?></span>
						<span class="author vcard"><?php the_author(); ?></span>
						<span class="meta-sep"><?php _e( ' on ', 'twentyten' ); ?> </span>
						<span class="entry-date"><?php echo get_the_date(); ?></span>
						<?php edit_post_link( __( 'Edit', 'twentyten' ), "<span class=\"meta-sep\">|</span>\n\t\t\t\t\t\t<span class=\"edit-link\">", "</span>\n\t\t\t\t\t" ); ?>
					</div><!-- .entry-meta -->


<hr width="50%" align="left" />				

<?php if ( get_the_author_meta( 'description' ) ) : // If a user has filled out their decscription show a bio on their entries  ?>
					<div id="entry-author-info">
						<div id="author-avatar">
							<?php echo get_avatar( get_the_author_meta( 'user_email' ), apply_filters( 'twentyten_author_bio_avatar_size', 60 ) ); ?>
						</div><!-- #author-avatar 	-->
						<div id="author-description">
							<h2><?php _e( 'About ', 'twentyten' ); ?><?php the_author(); ?></h2>
							<?php the_author_meta( 'description' ); ?>
							<div id="author-link">
								<?php the_author(); ?>
							</div><!-- #author-link	-->
						</div><!-- #author-description	-->
					</div><!-- .entry-author-info -->
<?php endif; ?>

					
				</div><!-- #post-<?php the_ID(); ?> -->
			<?php
				endwhile;
			?>	

<div id="nav-below" class="navigation">
					<div class="nav-previous"><?php next_posts_link( __( '<span class="meta-nav">&larr;</span> Older posts', 'twentyten' ) ); ?></div>
					<div class="nav-next"><?php previous_posts_link( __( 'Newer posts <span class="meta-nav">&rarr;</span>', 'twentyten' ) ); ?></div>
				</div><!-- #nav-below -->


	</div> <!-- END .yui-u .first .maincol-->
<?php get_sidebar(); ?>

</div> <!-- END .yui-gc -->
</div> <!-- END #bd -->
<?php get_footer(); ?>